package Lesson2;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {

        int max1 = -32768;
        int max2 = -32768;
        int n = 3;
        int[] Numbers;
        Numbers = new int[n];
        Scanner in = new Scanner(System.in);

        for (int i = 0; i < n; i++) {
            System.out.print("input" + (i + 1) + ": ");
            Numbers[i] = in.nextInt();
        }
        for (int i = 0; i < n; i++) {
            if (max1 == Numbers[i])
                max2 = Numbers[i];
            if (max1 < Numbers[i])
                max1 = Numbers[i];
        }
        for (int i = 0; i < n; i++) {
            if (max2 < Numbers[i] && Numbers[i] != max1)
                max2 = Numbers[i];
        }
        System.out.println("max1^2 = " + max1 * max1 + "\n" + "max2^2 = " + max2 * max2);
    }
}

