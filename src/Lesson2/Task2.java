package Lesson2;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("input a: ");
        int a = in.nextInt();
        System.out.print("input b: ");
        int b = in.nextInt();
        System.out.print("input c: ");
        int c = in.nextInt();

        double x1, x2;
        double D = b * b - 4 * a * c;
        if (D > 0) {
            x1 = ((-b) + Math.sqrt(D)) / (2 * a);
            x2 = ((-b) - Math.sqrt(D)) / (2 * a);
            System.out.println("x1 = " + x1 + "; x2 = " + x2);
        } else if (D == 0) {
            x1 = (-b) / (2 * a);
            System.out.println("x = " + x1);
        } else
            System.out.println("Уравнение не имеет корней");
    }
}
