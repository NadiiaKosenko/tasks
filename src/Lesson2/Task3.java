package Lesson2;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = 3;
        String[] rows;
        rows = new String[n];
        int[] len;
        len = new int[n];
        int min = 32767;
        int number = 0;

        for (int i = 0; i < n; i++) {
            System.out.print("input row " + (i + 1) + ": ");
            rows[i] = in.nextLine();
        }

        for (int i = 0; i < n; i++) {
            len[i] = rows[i].length();
            if (len[i] < min) {
                min = len[i];
                number = i;
            }
        }
        System.out.print(rows[number] + " ; " + min);
    }
}