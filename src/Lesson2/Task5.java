package Lesson2;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = 3;
        String[] rows;
        rows = new String[n];
        String buf;
        int[] len;
        len = new int[n];

        for (int i = 0; i < n; i++) {
            System.out.print("input row " + (i + 1) + ": ");
            rows[i] = in.nextLine();
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < (n - 1); j++) {
                len[j] = rows[j].length();
                len[j + 1] = rows[j + 1].length();
                if (len[j] > len[j + 1]) {
                    buf = rows[j + 1];
                    rows[j + 1] = rows[j];
                    rows[j] = buf;
                }
            }
        }
        for (int i = 0; i < n; i++) {
            len[i] = rows[i].length();
            System.out.print(rows[i] + " ; " + len[i] + " сим.\n");
        }


    }
}
