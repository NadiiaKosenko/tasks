package Lesson2;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = 3;
        String[] rows;
        rows = new String[n];
        int[] len;
        len = new int[n];
        int avg;
        int sum = 0;

        for (int i = 0; i < n; i++) {
            System.out.print("input row " + (i + 1) + ": ");
            rows[i] = in.nextLine();
        }
        for (int i = 0; i < n; i++) {
            len[i] = rows[i].length();
            sum = len[i] + sum;
        }
        avg = sum / n;
        for (int i = 0; i < n; i++) {
            len[i] = rows[i].length();
            if (len[i] < avg) {

                System.out.print(rows[i] + " ; " + len[i] + " сим.\n");
            }
        }

    }
}
