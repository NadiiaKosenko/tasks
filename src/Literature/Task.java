package Literature;

import java.util.ArrayList;
import java.util.Scanner;

public class Task {
    public static void main(String[] args) {

        String tag1 = null;
        String tag2 = null;
        String tag3 = null;

        ArrayList<Literature> list1 = new ArrayList<>();
        ArrayList<Literature> list2 = new ArrayList<>();
        ArrayList<Literature> list3 = new ArrayList<>();

        Scanner in = new Scanner(System.in);
        System.out.println("Input first tag");
        tag1 = in.nextLine();
        System.out.println("Input first tag");
        tag2 = in.nextLine();
        System.out.println("Input third tag");
        tag3 = in.nextLine();

        list1.add(new Journal("Book", new String[]{"tag", "tag2", "tag3"}, 2016, "Petrov"));
        list1.add(new Newspaper("NP1", "Animals", new String[]{"tag7", "tag", "tag8"}));
        list2.add(new Newspaper("NP2", "Medicine", new String[]{"tag", "tag6", "tag8"}));
        list2.add(new Journal("Magazine1", new String[]{"tag1", "tag3", "tag4", "tag"}, 2000, "Ivanov"));

        for (int i = 0; i < list1.size(); i++) {
            for (int k = 0; k < list1.get(i).tag.length; k++) {
                if (list1.get(i).tag[k].equalsIgnoreCase(tag1) || list1.get(i).tag[k].equalsIgnoreCase(tag2) || list1.get(i).tag[k].equalsIgnoreCase(tag3)) {
                    list3.add(list1.get(i));
                }
            }
        }
        for (int i = 0; i < list2.size(); i++) {
            for (int k = 0; k < list2.get(i).tag.length; k++) {
                if (list2.get(i).tag[k].equalsIgnoreCase(tag1) || list2.get(i).tag[k].equalsIgnoreCase(tag2) || list2.get(i).tag[k].equalsIgnoreCase(tag3)) {
                    list3.add(list2.get(i));

                }
            }
        }
        getLiteratureFromList(list3);
    }

    public static void getLiteratureFromList(ArrayList<Literature> list3) {
        for (int i = 0; i < list3.size(); i++) {
            list3.get(i).getInfo();
        }
    }
}
