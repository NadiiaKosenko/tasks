package Literature;

public class Newspaper extends Literature {
    String subject;

    public Newspaper(String name, String subject, String[] tag) {
        super(name, tag);
        this.subject = subject;
    }

    @Override
    public void getInfo() {
        String t = "";
        for (String s : tag) {
            t = t + " " + s;
        }
        System.out.println("Newspaper " + name + ", subject: " + subject + ", TAGS:" + t);
    }
}