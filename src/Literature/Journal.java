package Literature;

public class Journal extends Literature {
    int year;
    String editor;

    public Journal(String name, String[] tag, int year, String editor) {
        super(name, tag);
        this.year = year;
        this.editor = editor;
    }

    @Override
    public void getInfo() {
        String t="";
        for (String s : tag) {
            t=t + " " + s;
        }
        System.out.println("Journal " + name + ", editor: " + editor + ", " + year + " year" + ", TAGS:" + t);
    }
}
