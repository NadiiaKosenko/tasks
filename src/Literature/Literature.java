package Literature;

public abstract class Literature {
    String name;
    String [] tag;

    public Literature(String name, String[] tag) {
        this.name = name;
        this.tag = tag;
    }
    public abstract void getInfo();
}

