package Lesson3;

import java.util.Scanner;

public class Rows6 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        String row;

        System.out.print("Enter row: ");
        row = in.nextLine();
        row = row.replaceAll("\\p{Punct}", "");
        row = row.replace(" ", "");

        if (row.equalsIgnoreCase(new StringBuilder(row).reverse().toString())) {
            System.out.println("Row is palindrome");
        } else {
            System.out.println("Row is not palindrome");
        }
    }
}
