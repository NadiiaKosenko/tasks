package Lesson3;

import java.util.Scanner;

public class Rows2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = 3;
        String[] Rows;
        Rows = new String[n];
        String text = "";
        String word;

        for (int i = 0; i < n; i++) {
            System.out.print("input row " + (i + 1) + ": ");
            Rows[i] = in.nextLine();
            text = text + Rows[i] + " ";
        }
        String[] Words = text.split(" ");
        int[] Amount;
        Amount = new int[Words.length];
        int minAmount = 10000;
        String result = null;
        for (int i = 0; i < Words.length; i++) {
            Amount[i] = Words[i].length();
            for (int j = 0; j < (Words[i].length() - 1); j++)
                for (int k = j + 1; k < Words[i].length(); k++) {
                    if (Words[i].charAt(j) == Words[i].charAt(k)) {
                        Amount[i] = Amount[i] - 1;
                        break;
                    }
                    if (Amount[i] < minAmount) {
                        minAmount = Amount[i];
                        result = Words[i];
                    } else if (Amount[i] == minAmount) {
                        result = result + ", " + Words[i];
                    }
                }
        }
        System.out.println("Minimum different symbols are in the word(s) " + result);
    }
}
