package Lesson3;

import com.sun.org.apache.xalan.internal.xsltc.compiler.Pattern;

import java.util.Scanner;

public class Rows3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = 3;
        String[] Rows;
        Rows = new String[n];
        String text = "";
        int k = 0;
        int result = 0;
        int vowels;
        int consonants;
        int amountEng=0;

        for (int i = 0; i < n; i++) {
            System.out.print("input row " + (i + 1) + ": ");
            Rows[i] = in.nextLine();
            text = text + Rows[i] + " ";
        }
        String[] Words = text.split(" ");
        String[] Eng;
        Eng = new String[Words.length];
        for (int i = 0; i < Words.length; i++) {
            if (Words[i].matches(("^[A-Za-z]+$"))) {
                Eng[k] = Words[i];
                amountEng++;
                //System.out.println(Eng[k]);
                k++;
            }
        }
        for (int i = 0; i < Eng.length; i++) {
            vowels = Eng[i].replaceAll("[^aeiou]", "").length();
            consonants = Eng[i].replaceAll("[aeiou]", "").length();
            if (vowels == consonants) {
                result++;
            }
        }
        System.out.println("Amount of words with only Latin alphabet letters - " + amountEng + ". \nThe same amount of vowels and consonants is in " + result + " words");

    }
}

