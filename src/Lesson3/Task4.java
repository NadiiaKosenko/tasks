package Lesson3;

import java.util.Locale;

public class Task4 {
    /**
     * This program count distance between two points
     */
    public static void main(String[] args) {

        /** Init array of points (one point is array of two coordinates: x and y)
         * for getting n point use points[n]
         * for getting y of first point use points[0][1], where 0 - number of point, 1 - y
         * for getting x of second point use points[1][0], where 1 - number of point, 0 - x
         */
        int[][] points = {
                {1, 1}, // first point (x, y)
                {2, 2}, // second point (x, y)
                {3, 3}, // third point (x, y)
                {4, 4}
        };

        //count and print distances
        countDistancesBetweenPoints(points);

    }

    /**
     * Method count distance between all points in array and print result
     * @param points - array of points
     */
    private static void countDistancesBetweenPoints(int[][] points) {
        double [] Sum;
        Sum = new double[points.length];
        double min = Double.MAX_VALUE;
        int number = 0;
        for (int first = 0; first < points.length; first++) {
            Sum [first]= 0;
            for (int second = 0; second < points.length; second++) {
                //count dx - diff between x of first point and x of second point
                int dx = points[first][0] - points[second][0];

                //count dy - diff between y of first point and y of second point
                int dy = points[first][1] - points[second][1];

                //count distance of two points
                double distance = Math.sqrt(dx * dx + dy * dy);

                Sum [first] = Sum [first] + distance;

            }
            if (Sum[first] < min) {
                min = Sum[first];
                number = first;
            }
        }
        System.out.println(points[number][0] + " "+ points[number][1]+ " " + Sum[number]);
    }

    /**
     * Method prints distance between two points
     * @param first - coordinates of the first point
     * @param second - coordinates of the second point
     * @param distance - distance between points
     */
    //private static void printResult(String first, String second, double distance) {
    //    System.out.println(String.format(Locale.US, "Distance between %s and %s is %.5f", first, second, distance));
   // }

    /**
     * Method generate string with point coordinates
     * @param point - array of point coordinates
     * @return formatted string with point coordinates
     */
    //private static String pointToString(int[] point) {
   //     return String.format(Locale.US, "[%d, %d]", point[0], point[1]);
   // }
}
