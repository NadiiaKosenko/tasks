package Lesson3;

import java.util.Locale;
import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        //init variables
        String USD = "USD";
        String EUR = "EUR";
        String RUB = "RUB";
        boolean convert = false;
        boolean condition = false;
        String BUY = "buy";
        String SELL = "sale";


//        float COURSE_PRIVAT_USD = 24.5f;   exchange rates sell
//        float COURSE_PRIVAT_EUR = 26f;
//        float COURSE_PRIVAT_RUB = 0.30f;
//        float COURSE_PUMB_USD = 25.4f;
//        float COURSE_PUMB_EUR = 27f;
//        float COURSE_PUMB_RUB = 0.25f;
//        float COURSE_UKRSIB_USD = 26.7f;
//        float COURSE_UKRSIB_EUR = 28.2f;
//        float COURSE_UKRSIB_RUB = 0.28f;

//        float COURSE_PRIVAT_USD = 25.5f;   exchange rates buy
//        float COURSE_PRIVAT_EUR = 27f;
//        float COURSE_PRIVAT_RUB = 0.20f;
//        float COURSE_PUMB_USD = 27.4f;
//        float COURSE_PUMB_EUR = 29f;
//        float COURSE_PUMB_RUB = 0.15f;
//        float COURSE_UKRSIB_USD = 28.7f;
//        float COURSE_UKRSIB_EUR = 29.2f;
//        float COURSE_UKRSIB_RUB = 0.18f;

        String[] Banks = {"Privat", "PUMB", "Ukrsib"};
        float[][] Course = {
                {24.5f, 26f, 0.30f, 25.f, 27f, 0.20f},
                {25.4f, 27f, 0.25f, 27.4f, 29f, 0.15f},
                {24.5f, 26f, 0.30f, 28, 7, 29.2f, 0.18f}
        };

        //init scanner
        Scanner scan = new Scanner(System.in);

        //enter amount of money
        System.out.println("Enter the amount of money that you want to change: ");
        int amount = scan.nextInt();

        //enter currency
        System.out.println("Enter the currency to convert (USD, EUR or RUB): ");
        String currency = scan.next();
        if (currency != USD || currency != EUR || currency != RUB) {
            System.err.println();
        }

        //enter bank
        System.out.println("Enter the bank name (Privat; PUMB or Ukrsib)");
        String bank = scan.next();

        //enter purchase or sale
        System.out.println("Enter type of the exchange operation (enter SELL or BUY)");
        String option = scan.next();


        //convert UAH to USD

        for (int i = 0; i < 3; i++) {
            if (option.equalsIgnoreCase(SELL)) {
                if (Banks[i].equalsIgnoreCase(bank) && USD.equalsIgnoreCase(currency)) {
                    System.out.println(String.format(Locale.US, "You money in %s: %.2f", USD, amount / Course[i][0]));
                    convert = true;
                    break;
                } else if (Banks[i].equalsIgnoreCase(bank) && EUR.equalsIgnoreCase(currency)) {
                    System.out.println(String.format(Locale.US, "You money in %s: %.2f", EUR, amount / Course[i][1]));
                    convert = true;
                    break;
                } else if (Banks[i].equalsIgnoreCase(bank) && RUB.equalsIgnoreCase(currency)) {
                    System.out.println(String.format(Locale.US, "You money in %s: %.2f", RUB, amount / Course[i][2]));
                    convert = true;
                    break;
                }
            }
            if (option.equalsIgnoreCase(BUY)) {
                if (Banks[i].equalsIgnoreCase(bank) && USD.equalsIgnoreCase(currency)) {
                    System.out.println(String.format(Locale.US, "You money in %s: %.2f", USD, amount / Course[i][3]));
                    convert = true;
                    break;
                } else if (Banks[i].equalsIgnoreCase(bank) && EUR.equalsIgnoreCase(currency)) {
                    System.out.println(String.format(Locale.US, "You money in %s: %.2f", EUR, amount / Course[i][4]));
                    convert = true;
                    break;
                } else if (Banks[i].equalsIgnoreCase(bank) && RUB.equalsIgnoreCase(currency)) {
                    System.out.println(String.format(Locale.US, "You money in %s: %.2f", RUB, amount / Course[i][5]));
                    convert = true;
                    break;
                }
            }
        }
        if (convert == false) {
            System.err.println("Can't convert to " + currency);
        }
    }
}

