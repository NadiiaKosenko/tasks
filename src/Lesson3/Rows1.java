package Lesson3;

import java.util.Scanner;

public class Rows1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String text;
        int amount; //колличество предложений
        int maxW = 0; // максимальное колличество слов
        int numW = 0; //номер предложения в котором больше всего слов
        int maxLit = 0; //максимальное колличество букв
        int numLit = 0; //номер преложения в котором больше всего букв


        System.out.print("input text: ");
        text = in.nextLine();

        String[] Sentences = text.split("\\.");

        amount = Sentences.length;
        System.out.println("Колличество педложений = " + amount);

        for (int i = 0; i < amount; i++) {
            int countW = 0;
            int countLit = 0;
            String[] Words;
            Words = Sentences[i].split(" ");
            for (int j = 0; j < Words.length; j++) {
                if (Words[j] != null) {
                    countW++;
                }
            }
            if (countW > maxW) {
                maxW = countW;
                numW = i;
            }
            for (int k = 0; k < Sentences[i].length(); k++) {
                if (Sentences[i].charAt(k) != ' ') {
                    countLit++;
                }
            }
            if (countLit > maxLit) {
                maxLit = countLit;
                numLit = i;
            }
        }
        System.out.println("Наибольшее колличество слов в предложении: " + Sentences[numW]);
        System.out.println("Наибольшее колличество букв в предложении: " + Sentences[numLit]);
    }
}
