package Lesson3;

import java.util.Locale;
import java.util.Scanner;

public class Task1 {
    /**
     * This program convert UAH to USD depending on selected bank
     */
    public static void main(String[] args) {

        //init variables
        float COURSE_PRIVAT = 24.5f;
        float COURSE_PUMB = 25f;
        float COURSE_UKRSIB = 26.7f;

        String[] Banks = {"Privat", "PUMB", "Ukrsib"};

        //init scanner
        Scanner scan = new Scanner(System.in);

        //enter amount of money
        System.out.println("Enter the amount of money that you want to change: ");
        int amount = scan.nextInt();

        //enter bank
        System.out.println("Enter the bank name (Privat; PUMB or Ukrsib)");
        String bank = scan.next();

        //convert UAH to USD
        if (Banks[0].equalsIgnoreCase(bank)) {
            System.out.println(String.format(Locale.US, "Your money is: %.2f USD", amount / COURSE_PRIVAT));
        } else if (Banks[1].equalsIgnoreCase(bank)) {
            System.out.println(String.format(Locale.US, "Your money is: %.2f USD", amount / COURSE_PUMB));
        } else if (Banks[2].equalsIgnoreCase(bank)) {
            System.out.println(String.format(Locale.US, "Your money is: %.2f USD", amount / COURSE_UKRSIB));
        } else {
            System.err.println("Can't convert");
        }
    }
}
