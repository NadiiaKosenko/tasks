package Lesson3;

import java.util.Scanner;

public class Rows5 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        String word;

        System.out.print("Enter word: ");
        word = in.nextLine();

        if (word.equalsIgnoreCase(new StringBuilder(word).reverse().toString())) {
            System.out.println("Word is palindrome");
        } else {
            System.out.println("Word is not palindrome");
        }
    }
}
