package Accounting;

public abstract class Employee {
    String name;
    String position;

    public Employee(String name, String position) {
        this.name = name;
        this.position = position;
    }

    public abstract double getMonthSalary();

    public abstract double getYearSalary();
}
