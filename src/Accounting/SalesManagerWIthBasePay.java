package Accounting;

public class SalesManagerWIthBasePay extends SalesManager {
    double basePay;

    public SalesManagerWIthBasePay(String name, double basePay, double commission, double sales, int period) {
        super(name, commission, sales, period);
        this.basePay = basePay;
    }

    //    public SalesManagerWIthBasePay(String name, double basePay, double commission, double sales, int period) {
//        super(name, position ,commission, sales, period);
//        this.basePay = basePay;
//    }

    @Override
    public double getMonthSalary() {
        return super.getMonthSalary() + basePay;
    }

    @Override
    public double getYearSalary() {
        return super.getYearSalary() + basePay * 12;
    }
}
