package Accounting;

public class Task {
    public static void main(String[] args) {

        Employee[] employees = new Employee[4];
        employees[0] = new DeputyDirector("Иванова Елена Львовна", 4500);
        employees[1] = new Designer("Вакуленко Дмитрий Владимирович", 7, 22, 27);
        employees[2] = new SalesManager("Коренькова Анна Павловна", 5, 50000 * 6 + 65000 * 6, 12);
        employees[3] = new SalesManagerWIthBasePay("Татьяна Сергеевна", 1000, 3, 50000 * 6 + 65000 * 6, 12);

        for (Employee employee : employees) {
            System.out.println("Месячный доход " + employee.position + " " + employee.name + ": " + employee.getMonthSalary() + " грн");
            System.out.println("Годовой доход " + employee.position + " " + employee.name + ": " + employee.getYearSalary() + " грн");

        }
    }
}
