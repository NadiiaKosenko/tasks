package Accounting;

public class SalesManager extends Employee {
    double commission;
    double sales;
    int period;

    public SalesManager(String name, double commission, double sales, int period) {
        super(name, "Sales Manager");
        this.commission = commission;
        this.sales = sales;
        this.period = period;
    }

    @Override
    public double getMonthSalary() {
        return sales * commission / (100 * period);
    }

    @Override
    public double getYearSalary() {
        return getMonthSalary() * 12;
    }
}
