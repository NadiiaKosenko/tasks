package Accounting;

public class Designer extends Employee {
    double hourSalary;
    int workingDays;
    double exchangeRate;

    public Designer(String name, double hourSalary, int workingDays, double exchangeRate) {
        super(name, "Designer");
        this.hourSalary = hourSalary;
        this.workingDays = workingDays;
        this.exchangeRate = exchangeRate;
    }

    @Override
    public double getMonthSalary() {
        return hourSalary * 8 * workingDays * exchangeRate;
    }

    @Override
    public double getYearSalary() {
        return hourSalary * 8 * 252 * exchangeRate;
    }
}

