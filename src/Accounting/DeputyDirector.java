package Accounting;

public class DeputyDirector extends Employee {

    int salary;

    public DeputyDirector(String name, int salary) {
        super(name, "Deputy Director");
        this.salary = salary;
    }

    @Override
    public double getMonthSalary() {
        return salary;
    }

    @Override
    public double getYearSalary() {
        return getMonthSalary() * 12;
    }
}
