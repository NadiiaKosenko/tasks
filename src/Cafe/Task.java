package Cafe;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Task {
    public static void main(String[] args) {

        Map<String, Double> menu = new HashMap<String, Double>();
        menu.put("dish1", 20.0d);
        menu.put("dish2", 35.4d);
        menu.put("dish3", 22.1d);
        menu.put("dish4", 10.7d);

        Map<String, Double> order = new HashMap<String, Double>();

        String n = "";
        int amount;
        double sum = 0d;

        for (Map.Entry entry : menu.entrySet()) {
            System.out.println(entry.getKey() + ", price: " + entry.getValue());

        }

        while (!n.equals("0")) {
            double price = 0d;
            int more = 0;
            Scanner in = new Scanner(System.in);
            System.out.println("Enter menu name");
            n = in.nextLine();

            price = menu.get(n);

            Scanner k = new Scanner(System.in);
            System.out.println("Enter amount");
            amount = k.nextInt();

            sum = sum + price * amount;
            order.put(amount + " " + n, menu.get(n) * amount);

            Scanner l = new Scanner(System.in);
            System.out.println("Will you order one more dish? (YES  - enter 1; NO - enter 2");
            more = l.nextInt();
            if (more == 2) {
                n = "0";
            }
        }
        System.out.println("Your order:");
        for (Map.Entry entry : order.entrySet()) {
            System.out.println(String.format(entry.getKey() + ", price: " + "%.2f", entry.getValue()));
        }
        System.out.println(String.format("Total: " + "%.2f", sum));
    }

}
