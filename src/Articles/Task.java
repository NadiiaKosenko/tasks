package Articles;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class Task {
    public static void main(String[] args) {

        ArrayList<Article> list1 = new ArrayList<>();
        ArrayList<Article> list2 = new ArrayList<>();

        list1.add(new Article("title1", new Date(2009, 10, 10), new String[]{"tag1, tag2"}));
        list1.add(new Article("title2", new Date(2000, 10, 10), new String[]{"tag1, tag2"}));
        list1.add(new Article("title3", new Date(2018, 10, 10), new String[]{"tag5, tag6"}));
        list2.add(new Article("title1", new Date(2009, 10, 10), new String[]{"tag1, tag2"}));
        list2.add(new Article("title4", new Date(2000, 10, 10), new String[]{"tag1, tag2"}));
        list2.add(new Article("title3", new Date(2018, 10, 10), new String[]{"tag7, tag9"}));

        System.out.println("Статьи, у которых совпадают заголовки и даты публикации:");
        for (int i = 0; i < list1.size(); i++) {
            for (int k = 0; k < list2.size(); k++) {
                if (list1.get(i).title.equals(list2.get(k).title) && list1.get(i).date.equals(list2.get(k).date)) {
                    list1.get(i).getInfo();
                    list2.get(k).getInfo();
                }
            }
        }
        System.out.println("В оба списка входят статьи:");
        for (int i = 0; i < list1.size(); i++) {
            for (int k = 0; k < list2.size(); k++) {
                if (list1.get(i).title.equals(list2.get(k).title) && Arrays.equals(list1.get(i).tags, list2.get(i).tags) && list1.get(i).date.equals(list2.get(k).date)) {
                    list1.get(i).getInfo();
                }
            }
        }
    }
}
