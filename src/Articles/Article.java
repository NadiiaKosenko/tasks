package Articles;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Article {
    String title;
    Date date;
    String [] tags;
    SimpleDateFormat df = new SimpleDateFormat("DD.MM.YYYY");

    public Article(String title, Date date, String[] tags) {
        this.title = title;
        this.date = date;
        this.tags = tags;
    }

    public void getInfo (){
        for (String tag : tags) {
            System.out.println(title + " " + "дата: " + df.format(date) + " " + tag);
        }
    }
}
