package Lesson5;

public class Books {
    String name;
    String author;
    int year;

    public Books(String name, String author, int year) {
        this.name = name;
        this.author = author;
        this.year = year;
    }
}
