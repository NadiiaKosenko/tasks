package Lesson5;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Task1 {
    public static void main(String[] args) {

        Books[] books = initBooks();
        Calendar c = new GregorianCalendar();
        int year = c.get(Calendar.YEAR);
        String b = null;


        b = getOldBookAuthor(books, year, b);

        System.out.println("The oldest book author(s): " + b);

        findBooksByAuthor(books, "Author1");

        booksPublishedBeforeYear(books, 2016);
    }

    public static String getOldBookAuthor(Books[] books, int year, String b) {
        for (int i = 0; i < books.length; i++) {
            if (books[i].year < year) {
                year = books[i].year;
                b = books[i].author;
            } else if (books[i].year == year && !b.contains(books[i].author)) {
                b = b + ", " + books[i].author;
            }
        }
        return b;
    }

    public static void booksPublishedBeforeYear(Books[] books, int y) {
        for (int i = 0; i < books.length; i++) {
            if (books[i].year < y) {
                System.out.println("Book: " + books[i].name + ", Author: " + books[i].author + ", year edition: " + books[i].year);
            }
        }
    }

    public static void findBooksByAuthor(Books[] books, String author) {
        for (int i = 0; i < books.length; i++) {
            if (books[i].author.equalsIgnoreCase(author)) {

                System.out.println(books[i].name);
            }
        }
    }

    public static Books[] initBooks() {
        Books[] books = new Books[4];
        books[0] = new Books("Book1", "Author1", 1990);
        books[1] = new Books("Book2", "Author2", 1996);
        books[2] = new Books("Book3", "Author1", 1990);
        books[3] = new Books("Book4", "Author1", 1996);
        return books;
    }
}
