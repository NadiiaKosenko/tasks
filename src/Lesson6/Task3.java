package Lesson6;

import java.util.List;

public class Task3 {
    public static void main(String[] args) {
        Figure[] listFigures = new Figure[7];
        listFigures[0] = new Triangle("Triangle", new int[]{3, 4, 5});
        listFigures[1] = new Rectangle("Rectangle", new int[]{2, 5});
        listFigures[2] = new Foursquare("Foursquare", new int[]{7});
        listFigures[3] = new Circle(4);
        listFigures[4] = new Circle(4);
        listFigures[5] = new Triangle("Triangle", new int[]{3, 4, 5});
        listFigures[6] = new Triangle("Triangle", new int[]{3, 4, 5});

        int amount = 0;

        getPolygonSquare(listFigures);

        getSquareIgnoreTriangle(listFigures);

        getPerimeterForPolygons(listFigures);

        getEqualFigures(listFigures, amount);

    }

    public static void getEqualFigures(Figure[] listFigures, int amount) {
        for (int i = 0; i < listFigures.length-1; i++) {

            for (int k = 1; k < listFigures.length && k!=i; k++) {
                if (listFigures[i].equals(listFigures[k])) {
                    amount++;
                    System.out.println(listFigures[i].name + " is equals to " + listFigures[k].name);
                }
            }
        }
        System.out.println("Total equal figures: " + amount);
    }

    public static void getPerimeterForPolygons(Figure[] listFigures) {
        for (int i = 0; i < listFigures.length; i++) {
            if (listFigures[i] instanceof Polygon) {
                Polygon polygon = (Polygon) listFigures[i];
                double perimeter = listFigures[i].getPerimeter();
                System.out.println(String.format("Perimeter of %s = %.2f", polygon.getName(), perimeter));
            }
        }
    }

    public static void getSquareIgnoreTriangle(Figure[] listFigures) {
        for (int i = 0; i < listFigures.length; i++) {
            if (listFigures[i] instanceof Circle) {
                Circle circle = (Circle) listFigures[i];
                double square = listFigures[i].getSquare();
                System.out.println(String.format("Square of %s = %.2f", circle.getName(), square));
            } else if (listFigures[i] instanceof Foursquare || listFigures[i] instanceof Rectangle) {
                Polygon polygon = (Polygon) listFigures[i];
                double square = listFigures[i].getSquare();
                System.out.println(String.format("Square of %s = %.2f", polygon.getName(), square));
            }
        }
    }

    public static void getPolygonSquare(Figure[] listFigures) {
        for (int i = 0; i < listFigures.length; i++) {
            if (listFigures[i] instanceof Polygon) {
                Polygon polygon = (Polygon) listFigures[i];
                double square = listFigures[i].getSquare();
                System.out.println(String.format("Square of %s = %.2f", polygon.getName(), square));
            }
        }
    }
}
