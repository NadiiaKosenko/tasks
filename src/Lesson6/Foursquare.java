package Lesson6;

public class Foursquare extends Polygon {
    public Foursquare(String name, int[] side) {
        super(name, side);
    }

    @Override
    public double getSquare() {
            return side [0] * side[0];
    }

    @Override
    public double getPerimeter() {
        return side [0] * 4;
    }
}
