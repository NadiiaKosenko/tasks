package Lesson6;

/**
 * Created by nadya on 14.12.16.
 */
public class Lion extends Cat {
     double roar = 0.05f;

    public Lion(String name, double weight, int height) {
        super(name, weight, height);
    }

    @Override
    public double getPowerFactor() {
        return super.getPowerFactor()+super.getPowerFactor()*roar;
    }
}
