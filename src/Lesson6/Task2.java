package Lesson6;

import java.util.Random;

public class Task2 {
    public static void main(String[] args) {

        //create cats
        Cat chiropractor = new Cat("Chip", 7, 50);
        Cat godzilla = new Cat("Godzilla", 8, 50);
        Lion lion = new Lion("Lion", 8, 50);

        //start fighting
        System.out.printf("%s vs %s", lion.name, godzilla.name).println();

        //get winner
        String winnerName = getWinner(lion, godzilla);

        //print winner name
        System.out.printf("%s wins", winnerName);
    }

    /**
     * This method compare to cats and return winner
     *
     * @param catInRedCorner   - first cat
     * @param catInBlackCorner - second cat
     * @return winner
     */
    static String getWinner(Cat catInRedCorner, Cat catInBlackCorner) {
        if (catInBlackCorner.getPowerFactor()+getLuck() > catInRedCorner.getPowerFactor()+getLuck()) {
            return catInBlackCorner.name;
        } else {
            return catInRedCorner.name;
        }
    }

    public static int getLuck() {
        Random r = new Random();
        int l;
            l = r.nextInt(201) - 100;
            //System.out.println(l);
        return l;
    }
}
