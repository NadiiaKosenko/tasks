package Lesson6;

public class Triangle extends Polygon {
    public Triangle(String name, int[] side) {
        super(name, side);
    }
    @Override
    public double getSquare() {

        //считаем полупериметр
        double p = (side[0] + side[1] + side[2]) / 2;

        //считаем площать треугольника по формуле герона
        double square = Math.sqrt(p * (p - side[0]) * (p - side[1]) * (p - side[2]));

        //возвращаем значение
        return square;
    }

    @Override
    public double getPerimeter() {
        return side[0] + side[1] + side[2];
    }
}
