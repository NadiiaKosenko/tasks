package Lesson6;

public class Task1 {
    public static void main(String[] args) {

        int[][] points = new int[][]{
                {2, 1}, // first point (x, y)
                {3, 3}, // second point (x, y)
                {2, 5}, // third point (x, y)
                {1, 9},
                {4, 8},
                {5, 2},
                {6, 6},
                {6, 11},
                {7, 1},
                {9, 7}
        };

        int amount = fact(10) / ((fact(10 - 3)) * fact(3));
        System.out.println(amount);

        System.out.println(secondWay());
    }

    public static int secondWay() {
        int sum = 0;
        for (int i = 1; i < 9; i++) {
            sum = sum + i * (i + 1) / 2;
        }
        return sum;
    }

    public static int fact(int n) {
        int result = n;
        if (result == 1) {
            return 1;
        } else result = fact(n - 1) * n;
        return result;
    }
}

