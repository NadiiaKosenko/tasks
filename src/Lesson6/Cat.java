package Lesson6;

public class Cat {
    String name;
    double weight;
    int height;


    public Cat(String name, double weight, int height) {
        this.name = name;
        this.weight = weight;
        this.height = height;
    }

    /**
     * Method calculate cats power factor
     *
     * @return power factor
     */
    public double getPowerFactor() {
        return weight * height;
    }

}

