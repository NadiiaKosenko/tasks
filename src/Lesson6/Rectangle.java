package Lesson6;

public class Rectangle extends Polygon{
    public Rectangle(String name, int[] side) {
        super(name, side);
    }

    @Override
    public double getSquare() {
        return side[0] * side[1];
    }

    @Override
    public double getPerimeter() {
        return (side[0] + side[1])*2;
    }
}
