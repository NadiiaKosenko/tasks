package Lesson6;

public class Figure {
    String name;
    int square;
    int [] side;

    public int[] getSide() {
        return side;
    }

    public Figure(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public double getSquare() {
        return 0;
    }

    public double getPerimeter() {
        return 0;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Figure figure = (Figure) o;

        if (square != figure.square) return false;
        return name != null ? name.equals(figure.name) : figure.name == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + square;
        return result;
    }
}
