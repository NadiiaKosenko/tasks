package TaskLibrary;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class Task {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Введите количество лет за которые необходимо вывести список литературы");
        int n = in.nextInt()-1;

        Library[] library = new Library[3];
        library[0] = new Book("Book1", "Author1", "publishingHous1", 2016);
        library[1] = new Yearbook("Yearbook1", "Animals", "publishingHouse2", 2014);
        library[2] = new Magazine("Magazine1", "Medicine", new GregorianCalendar(2007, 10, 10));

        for (Library library1 : library) {
            int filterYear = Calendar.getInstance().get(Calendar.YEAR) - n;
            if (library1.getYear() >= filterYear) {
                library1.getInfo();
            }
        }
    }
}

