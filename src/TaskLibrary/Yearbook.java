package TaskLibrary;

public class Yearbook extends  Library {
    String subject;
    String publishingHouse;
    int year;

    public Yearbook(String name, String subject, String publishingHouse, int year) {
        super(name);
        this.subject = subject;
        this.publishingHouse = publishingHouse;
        this.year = year;
    }

    @Override
    public void getInfo() {
        System.out.println("Ежегодник: " + name + ", тематика: " + subject + ", издательство: " + publishingHouse + ", год издания: " + year);
    }

    @Override
    public int getYear() {
        return year;
    }
}
