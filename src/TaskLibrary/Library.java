package TaskLibrary;

import java.text.SimpleDateFormat;

public abstract class Library {
    String name;

    public Library(String name) {
        this.name = name;
    }

    public abstract void getInfo();
    public abstract int getYear();
}
