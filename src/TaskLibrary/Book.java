package TaskLibrary;

import java.time.Year;

public class Book extends Library {
    String author;
    String publishingHouse;
    int year;

    public Book(String name, String author, String publishingHouse, int year) {
        super(name);
        this.author = author;
        this.publishingHouse = publishingHouse;
        this.year = year;
    }

    @Override
    public void getInfo() {
        System.out.println("Книга: " + name + ", автор: " + author + ", издательство: "+ publishingHouse + ", год издания: " + year);
    }

    @Override
    public int getYear() {
        return year;
    }
}
