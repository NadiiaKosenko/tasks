package TaskLibrary;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Magazine extends Library {
    String subject;
    Calendar date;



    public Magazine(String name, String subject, Calendar date) {
        super(name);
        this.subject = subject;
        this.date = date;
    }


    @Override
    public void getInfo() {
        System.out.println("Журнал: " + name + ", тематика: " + subject + ", дата выхода в печать:  " + date.get(Calendar.YEAR) + "." + date.get(Calendar.MONTH)+ "." + date.get(Calendar.DATE));
    }

    @Override
    public int getYear() {
        return date.get(Calendar.YEAR);
    }

}
